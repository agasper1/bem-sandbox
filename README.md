## Block, Element, Modifier (BEM) Sandbox 
- Fiddling with BEM style for consistent brand experience across platforms
    - web
    - app


## References
- [BEM ToolBox](https://en.bem.info/toolbox/)
- [BEM Key Concepts](https://en.bem.info/methodology/key-concepts/)